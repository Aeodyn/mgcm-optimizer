numpy~=1.19.4
ortools~=9.3.10497
pandas~=1.2.1
PyYAML~=6.0
tabulate~=0.8.9
xarray~=0.21.1
