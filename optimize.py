"""MGCM Optimizer v4.8.1 by Aeodyn @ nya research
https://discord.gg/DXzb6gQ
"""


import numpy as np
import pandas as pd
import xarray as xr
from ortools.sat.python import cp_model
from math import ceil, prod, log, exp
from yaml import dump as dump_yaml
import gettext
import os
import sys
import re
from tabulate import tabulate
from json import dump as dump_json
from urllib.parse import quote as quote_url


# This determines how precise the end score is, 1000 = 3 decimal places. Reducing this can possibly increase speed.
MULTS_SCALE = 1
# To fix rounding issues; shouldn't need to be modified.
SPEED_SCALE = 20
STAT_SCALE = 20

try:
	sys.stdout.reconfigure(encoding='utf-8')
except AttributeError:
	pass


# PyInstaller
try:
	LOCALEPATH = os.path.join(sys._MEIPASS, 'locale')
except Exception:
	LOCALEPATH = os.path.join(os.path.abspath("."), 'locale')

if os.getenv('LANG') is None:
	import locale
	lang, enc = locale.getdefaultlocale()
	if lang:
		os.environ['LANG'] = lang
try:
	translation = gettext.translation('messages', LOCALEPATH)
	_ = translation.gettext
except:
	_ = lambda x: x


# Each element is turned into a positional bit for easy comparison.
int2elem = np.array(['fire', 'lightning', 'water', 'light', 'dark', 'joker'])
elem2flag = xr.DataArray(pd.Series({'fire': 1, 'lightning': 2, 'water': 4, 'light': 8, 'dark': 16, 'joker': 31}, name='elem_flags')).rename({'dim_0': 'element'})


def linterp(x0, x1, y0, y1, x):
	return ((x1 - x)*y0 + (x - x0)*y1)/(x1 - x0)

def parse_bulk_factor(s):
	if s == 'bulk':
		return 1
	x = re.match('^bulk-(.*)', s)[1]
	if x == 'dd':
		return 0.3
	elif x == 'du':
		return 1.7
	else:
		return float(x)

def load_csv(path, **kwargs):
	if isinstance(path, str):
		with open(path, encoding='utf-8') as file:
			return pd.read_csv(file, **kwargs)
	else:
		return pd.read_csv(path, **kwargs)

def load_compat(path, dresses):
	girls = np.array(sorted(set(dresses['girl'].values)))
	r = xr.DataArray(girls[:, None] == girls[None, :], coords={'main': girls, 'sub': girls})

	if not path:
		return r
	if isinstance(path, str):
		try:
			with open(path, encoding='utf-8') as file:
				lines = [re.sub(r'\s+', '', line) for line in file]
		except FileNotFoundError:
			return r
	else:
		lines = [re.sub(r'\s+', '', line) for line in path]

	for line in lines:
		parts = line.split('#')[0].split(':')
		if len(parts) == 0 or parts == ['']:
			continue
		if (len(parts) != 2):
			raise Exception(_('Error parsing {path}.').format(path=path))
		for m in parts[0].split(','):
			if int(m) in girls:
				r.loc[{'main': int(m), 'sub': [int(s) for s in parts[1].split(',') if int(s) in girls]}] = True
	return r

def relevel_dresses(dresses):
	dresses['maxLVL'] = 60 + 5*dresses['LB']
	toUpdate = dresses['dress'].to_index().intersection(dresses['dress'].where(dresses['LVL'] != dresses['maxLVL']).to_index())
	filtered = dresses.loc[{'dress': toUpdate}]
	dresses['HP'].loc[{'dress': toUpdate}] = linterp(filtered['LVL'], -38.5, filtered['HP'], 0, filtered['maxLVL'])
	dresses['ATK'].loc[{'dress': toUpdate}] = linterp(filtered['LVL'], -38.5, filtered['ATK'], 0, filtered['maxLVL'])
	dresses['DEF'].loc[{'dress': toUpdate}] = linterp(filtered['LVL'], -38.5, filtered['DEF'], 0, filtered['maxLVL'])
	dresses['AGI'].loc[{'dress': toUpdate}] = filtered['AGI'] + (filtered['maxLVL'] - filtered['LVL'])*3/5
	return

def load_orbs(path, types=None):
	t_orbs = load_csv(path, usecols=['rarity', 'type', 'main_stat', 'HP', 'ATK', 'DEF', 'AGI', 'FCS', 'RES', 'HP%', 'ATK%', 'DEF%', 'ALL%', 'CC', 'CD', 'element', 'ATK_up', 'DEF_up', 'uid'], dtype={'element': str}).fillna(0)
	t_orbs.dropna(how='all', inplace=True)
	t_orbs.rename(columns={'uid': 'UserRuneId'}, inplace=True)
	t_orbs['CC'] = t_orbs['CC'].astype(int)
	t_orbs['CD'] = t_orbs['CD'].astype(int)
	if types is not None:
		t_orbs = t_orbs[t_orbs['type'].isin(types)]
	for i, row in t_orbs.iterrows():
		t_orbs.at[i, row['type']] = row['main_stat']  # Copy the main stat over to the usual column so everything is uniform.
	t_orbs['elem_flags'] = np.array([sum([2**(int(x)-2) for x in str(elem)]) for elem in t_orbs['element']], dtype=int)  # Convert the decimal digits respresenting sub-elements to a sum of the above binary flags.
	t_orbs.index.name = 'orb'
	return xr.Dataset(t_orbs)

# The total wardrobe available to the player.
def load_dresses(path):
	t_dresses = load_csv(path, usecols=['id', 'dress', 'rarity', 'girl', 'element', 'LVL', 'LB', 'HP', 'ATK', 'DEF', 'FCS', 'RES', 'AGI', 'joker', 'beast', 'bs1', 'bs2', 'uid'], dtype={'element': str, 'HP': float, 'ATK': float, 'DEF': float, 'FCS': float, 'RES': float, 'AGI': float}, index_col='id').fillna(0)
	t_dresses.dropna(how='all', inplace=True)
	t_dresses.rename(columns={'dress': 'name', 'uid': 'UserCardId'}, inplace=True)
	t_dresses.index.names = ['dress']
	t_dresses = t_dresses.sort_values(['dress', 'LVL'], ascending=[True, False])
	t_dresses = t_dresses.groupby('dress').first()
	t_dresses['elem_flags'] = np.array([sum([2**(int(x)-2) for x in str(elem)]) for elem in t_dresses['element']], dtype=int)  # Again, construct the binary flags from the decimal digits.
	dresses = xr.Dataset(t_dresses)
	dresses['subelem_flags'] = dresses['elem_flags'] | (elem2flag.loc['joker'].item() * dresses['joker'])  # Set the element flags for joker dresses here and not earlier because it only applies as a sub-dress, not as a main dress.
	return dresses

# The four main units to use.
def load_mults(dresses, path, is_url=False):
	columns = ['HP','ATK','DEF','AGI','FCS','RES','CC','CD','CCCD','CC-ATK','CC-HP','CC-DEF','CC-AGI','CC-FCS','CC-RES','CD-ATK','CD-HP','CD-DEF','CD-AGI','CD-FCS','CD-RES','CCCD-ATK','CCCD-HP','CCCD-DEF','CCCD-AGI','CCCD-FCS','CCCD-RES','bulk','S4-HP','S4-ATK','S4-DEF','S4-AGI','S4-FCS','S4-RES', 'ATK_up-ATK', 'DEF_up-DEF']
	t_mults = xr.Dataset((pd.read_csv if is_url else load_csv)(path, index_col='unit', usecols=lambda x: (x in ['unit', 'dress']+columns) or x.startswith('bulk-'), dtype={**{x: float for x in columns}, **{'unit': str}}).dropna(how='all').reindex(columns=['dress']+columns, fill_value=0.0).fillna(0).sort_index()).rename_vars({'dress': 'name'})
	t_mults = t_mults.where(t_mults['name'].copy(data=[isinstance(t, str) and t != '' for t in t_mults['name'].values]), drop=True)
	t_mults['ATK_up'] = xr.zeros_like(t_mults['ATK'])
	t_mults['DEF_up'] = xr.zeros_like(t_mults['DEF'])
	t_mults['HP'] += sum(t_mults[x] for x in t_mults.data_vars if x.startswith('bulk'))
	mults = t_mults[['HP', 'ATK', 'DEF', 'AGI', 'FCS', 'RES', 'CC', 'CD', 'CCCD', 'ATK_up', 'DEF_up', 'ATK_up-ATK', 'DEF_up-DEF']].to_array().rename({'variable': 'stat'})  # Package stats for each unit together into one "stats" variable for easier use later.
	linearized = xr.zeros_like(mults, dtype=float).expand_dims({'linstat': mults['stat'].rename({'stat': 'linstat'})}).copy()
	t_cc = t_mults[['CC-HP', 'CC-ATK', 'CC-DEF', 'CC-AGI', 'CC-FCS', 'CC-RES']].rename({'CC-HP':'HP', 'CC-ATK':'ATK', 'CC-DEF':'DEF', 'CC-AGI':'AGI', 'CC-FCS':'FCS', 'CC-RES':'RES'}).to_array().reindex({'variable': mults['stat'].values}, fill_value=0.0)
	t_cd = t_mults[['CD-HP', 'CD-ATK', 'CD-DEF', 'CD-AGI', 'CD-FCS', 'CD-RES']].rename({'CD-HP':'HP', 'CD-ATK':'ATK', 'CD-DEF':'DEF', 'CD-AGI':'AGI', 'CD-FCS':'FCS', 'CD-RES':'RES'}).to_array().reindex({'variable': mults['stat'].values}, fill_value=0.0)
	t_cccd = t_mults[['CCCD-HP', 'CCCD-ATK', 'CCCD-DEF', 'CCCD-AGI', 'CCCD-FCS', 'CCCD-RES']].rename({'CCCD-HP':'HP', 'CCCD-ATK':'ATK', 'CCCD-DEF':'DEF', 'CCCD-AGI':'AGI', 'CCCD-FCS':'FCS', 'CCCD-RES':'RES'}).to_array().reindex({'variable': mults['stat'].values}, fill_value=0.0)
	t_cccd = (t_cccd-t_cc-t_cd)/625  # Bilinear interpolation.
	t_cc = t_cc/25
	t_cd = t_cd/25
	linearized.loc[{'stat': 'CC'}] += t_cc.rename({'variable': 'linstat'})
	linearized.loc[{'linstat': 'CC'}] += t_cc.rename({'variable': 'stat'})
	linearized.loc[{'stat': 'CD'}] += t_cd.rename({'variable': 'linstat'})
	linearized.loc[{'linstat': 'CD'}] += t_cd.rename({'variable': 'stat'})
	linearized.loc[{'stat': 'CCCD'}] += t_cccd.rename({'variable': 'linstat'})
	linearized.loc[{'linstat': 'CCCD'}] += t_cccd.rename({'variable': 'stat'})
	t_bulk = sum(parse_bulk_factor(x)*t_mults[x] for x in t_mults.data_vars if x.startswith('bulk'))/750
	linearized.loc[{'stat': 'HP', 'linstat': 'DEF'}] += t_bulk
	linearized.loc[{'stat': 'DEF', 'linstat': 'HP'}] += t_bulk
	linearized.loc[{'stat': 'ATK', 'linstat': 'ATK_up'}] += t_mults['ATK_up-ATK']
	linearized.loc[{'stat': 'ATK_up', 'linstat': 'ATK'}] += t_mults['ATK_up-ATK']
	linearized.loc[{'stat': 'DEF', 'linstat': 'DEF_up'}] += t_mults['DEF_up-DEF']
	linearized.loc[{'stat': 'DEF_up', 'linstat': 'DEF'}] += t_mults['DEF_up-DEF']

	missing = [t for t in t_mults['name'].values if t not in dresses['name'].values]
	if missing:
		raise KeyError('Missing dresses: ' + ', '.join(missing))
	units = dresses.swap_dims({'dress': 'name'}).loc[{'name': t_mults['name']}].reset_coords(['dress', 'name'])
	if len(set(units['girl'].values)) != len(units['unit']):
		raise ValueError("Can't use the same girl as main dress more than once.")

	units['element'].values = int2elem[pd.to_numeric(units['element'])-2]
	s4 = t_mults[['S4-ATK','S4-HP','S4-DEF','S4-AGI','S4-FCS','S4-RES']].rename({'S4-HP':'HP', 'S4-ATK':'ATK', 'S4-DEF':'DEF', 'S4-AGI':'AGI', 'S4-FCS':'FCS', 'S4-RES':'RES'}).to_array().reindex({'variable': mults['stat'].values}, fill_value=0.0).rename({'variable': 'stat'})

	units['beastHP'] = (units['bs1'] == 1) | (units['bs2'] == 1)
	units['beastATK'] = (units['bs1'] == 2) | (units['bs2'] == 2)
	units['beastDEF'] = (units['bs1'] == 3) | (units['bs2'] == 3)
	units['beastFCS'] = (units['bs1'] == 4) | (units['bs2'] == 4)
	units['beastRES'] = (units['bs1'] == 5) | (units['bs2'] == 5)
	return (units, mults, linearized, s4)

def update_mults(mults0, linearized, stats):
	return mults0 + xr.dot(linearized, stats, dims=['stat']).rename({'linstat': 'stat'})/2  # Divided by 2 because each product mult is put in two places in the 'linearized' array; without this, calculated bulk for example is off by factor of 2.

def load_minmax_stats(minfile, maxfile):
	columns0 = ['HP','ATK','DEF','AGI','FCS','RES','CC','CD','CCCD','bulk','ATK_up','DEF_up']
	minstats = maxstats = xr.DataArray(0).expand_dims({'unit':np.array([], dtype=str), 'stat':columns0}).to_dataset('stat')
	if minfile is not None:
		try:
			tdf = load_csv(minfile, index_col='unit', usecols=lambda x: (x in ['unit']+columns0) or x.startswith('bulk-'))
			columns = np.unique(np.concatenate([tdf.columns, columns0]))
			minstats = xr.Dataset(tdf.dropna(how='all').reindex(columns=columns, fill_value=0).fillna(0).sort_index())
		except FileNotFoundError:
			pass
	if maxfile is not None:
		try:
			tdf = load_csv(maxfile, index_col='unit', usecols=lambda x: (x in ['unit']+columns0) or x.startswith('bulk-'))
			columns = np.unique(np.concatenate([tdf.columns, columns0]))
			maxstats = xr.Dataset(tdf.dropna(how='all').reindex(columns=columns, fill_value=0).fillna(0).sort_index())
		except FileNotFoundError:
			pass
	return (minstats, maxstats)

def load_add_stats(addfile, units):
	stats = ['HP','ATK','DEF','AGI','FCS','RES','CC','CD','CCCD','ATK_up','DEF_up']
	elements = ['all','fire','lightning','water','light','dark']
	if addfile is not None:
		try:
			df = load_csv(addfile, index_col='element', usecols=lambda x: x in ['element']+stats, dtype=str)
			df.dropna(how='all', inplace=True)
			df = df.apply(lambda x: x.str.rstrip('%'))
			df[df == ''] = '0'
			elemstats = xr.DataArray(df.astype(int)).rename({'dim_1': 'stat'}).reindex({'stat': stats, 'element': elements}).fillna(0).astype(int)
			r = (elemstats.sel(element='all') + elemstats.sel(element=units['element']))
			r.loc[{'stat': 'CCCD'}] = r.loc[{'stat': 'CC'}]*r.loc[{'stat': 'CD'}]
		except FileNotFoundError:
			r = xr.DataArray(0).expand_dims({'unit': units['unit'], 'stat': stats}).copy()
	else:
		r = xr.DataArray(0).expand_dims({'unit': units['unit'], 'stat': stats}).copy()

	r.loc[{'stat': 'FCS'}] += np.ceil(units['beastFCS'] * 40).astype(int)
	r.loc[{'stat': 'RES'}] += np.ceil(units['beastRES'] * 40).astype(int)
	return r

def load_url(dresses, url):
	prefix = r'docs.google.com/spreadsheets/d/'
	match = re.search(re.escape(prefix) + r'([\w\-]+)/edit', url)
	if not match:
		raise ValueError(_('Invalid URL.'))
	sheet_id = match.group(1)
	mults_page = quote_url(r'Results/成绩')
	order_page = quote_url(r'order.txt')
	turns_page = quote_url(r'Turns/回合')
	base = f'https://{prefix}{sheet_id}/gviz/tq?tqx=out:csv&sheet='

	try:
		mults = load_mults(dresses, base + mults_page, True)
	except TypeError:
		raise ValueError(_('Sheet must be publicly-readable.'))

	try:
		order_df = pd.read_csv(base + order_page)
		if 'order.txt:' in order_df:
			order = [re.sub(r'\s+', '', line) for line in order_df.values.flatten() if isinstance(line, str)]
		else:
			try:
				order_df = pd.read_csv(base + turns_page + '&range=B16:E22')
				if 'order.txt:' in order_df:
					order = [re.sub(r'\s+', '', line) for line in order_df.values.flatten() if isinstance(line, str)]
				else:
					order = None
			except pd.errors.EmptyDataError:
				order = None
	except pd.errors.EmptyDataError:
		try:
			order_df = pd.read_csv(base + turns_page + '&range=B16:E22')
			if 'order.txt:' in order_df:
				order = [re.sub(r'\s+', '', line) for line in order_df.values.flatten() if isinstance(line, str)]
			else:
				order = None
		except pd.errors.EmptyDataError:
			order = None


	return (mults, order)

def get_filter_types(mults, minstats):
	tmin = minstats.to_array().rename({'variable': 'stat'})
	multstypes = mults['stat'][mults.any('unit')].values
	mintypes = tmin['stat'][tmin.any('unit')].values
	if any(x.startswith('bulk') for x in multstypes) or any(x.startswith('bulk') for x in mintypes):
		types = np.unique(np.concatenate([multstypes, mintypes, ['AGI', 'HP', 'DEF']]))
	else:
		types = np.unique(np.concatenate([multstypes, mintypes, ['AGI']]))
	return np.concatenate([types, [x+"%" for x in types]])

def load_speed_orders(ordfile):
	if ordfile is None:
		return []
	if isinstance(ordfile, str):
		try:
			with open(ordfile, encoding='utf-8') as file:
				return [re.sub(r'\s+', '', line) for line in file]
		except FileNotFoundError:
			return []
	else:
		return [re.sub(r'\s+', '', line) for line in ordfile]

def apply_speed_orders(speed_orders, model, speedvars):
	old_labels = {'a': '0', 'b': '1', 'c': '2', 'd': '3'}
	for line in speed_orders:
		t = re.split(r'([><=≥≤]=?)', line)
		comparators = t[1::2]
		expressions = t[::2]
		sums = []
		sums1 = []
		for e in expressions:
			terms = e.split('+')
			products = []
			products1 = []
			for t in terms:
				factors = t.split('*')
				tfs = []
				tfs1 = []
				scaled = False
				for f in factors:
					try:
						tfs.append(int(f))
						tfs1.append(int(f))
					except ValueError:
						if f in speedvars:
							tfs.append(speedvars[f])
							tfs1.append(speedvars[f]+SPEED_SCALE)
							scaled = True
						elif f in old_labels and old_labels[f] in speedvars:
							tfs.append(speedvars[old_labels[f]])
							tfs1.append(speedvars[old_labels[f]]+SPEED_SCALE)
							scaled = True
						else:
							raise Exception(_('Error parsing order file.'))
				if not scaled:
					tfs.append(SPEED_SCALE)
					tfs1.append(SPEED_SCALE)
				products.append(prod(tfs))
				products1.append(prod(tfs1))
			sums.append(sum(products))
			sums1.append(sum(products1))
		for i in range(len(comparators)):
			c = comparators[i]
			if c == '>':
				model.Add(sums[i] > sums1[i+1])
			elif c == '<':
				model.Add(sums1[i] < sums[i+1])
			elif c == '>=' or c == '≥':
				model.Add(sums[i] >= sums[i+1])
			elif c == '<=' or c == '≤':
				model.Add(sums[i] <= sums[i+1])
			elif c == '=' or c == '==':
				model.Add(sums1[i] > sums[i+1])
				model.Add(sums[i] < sums1[i+1])
			else:
				raise Exception(_('Error parsing order file.'))


def apply_orbs(units, dresses, orbs, compat):
	# Stat calculation for each dress/orb combination.
	withorbs = dresses.copy()
	# From testing, ceiling is applied to each dress after orbs, but before the slot is applied.
	withorbs['HP'] = np.ceil(withorbs['HP'] * (1 + (orbs['HP%']+orbs['ALL%'])/100) + orbs['HP'])
	withorbs['ATK'] = np.ceil(withorbs['ATK'] * (1 + (orbs['ATK%']+orbs['ALL%'])/100) + orbs['ATK'])
	withorbs['DEF'] = np.ceil(withorbs['DEF'] * (1 + (orbs['DEF%']+orbs['ALL%'])/100) + orbs['DEF'])
	withorbs['AGI'] = np.ceil(withorbs['AGI'] * (1 + orbs['ALL%']/100) + orbs['AGI'])
	withorbs['FCS'] = np.ceil(withorbs['FCS'] * (1 + orbs['ALL%']/100) + orbs['FCS'])
	withorbs['RES'] = np.ceil(withorbs['RES'] * (1 + orbs['ALL%']/100) + orbs['RES'])
	withorbs['CC'] = orbs['CC']
	withorbs['CD'] = orbs['CD']
	withorbs['ATK_up'] = orbs['ATK_up']
	withorbs['DEF_up'] = orbs['DEF_up']
	withorbs['stats'] = withorbs[['ATK', 'HP', 'DEF', 'AGI', 'FCS', 'RES', 'CC', 'CD', 'ATK_up', 'DEF_up']].to_array().rename({'variable': 'stat'})
	withorbs['subelem_flags'] = withorbs['subelem_flags'] | orbs['elem_flags']  # This is why binary flags are used.
	# Finally taking the unit into consideration, instead of just dress and orb.
	withorbs['compat'] = (20 + 70*(units['dress'] == withorbs['dress']) + 5*(compat.loc[{'main': units['girl'], 'sub': withorbs['girl']}] | withorbs['beast']) + 5*(units['elem_flags'] & ~withorbs['subelem_flags'] == 0))/100  # Handles the 20% base of being a subdress, the 5% bonuses from matching girl and/or element, and +70% bonus from actually being the main dress.
	r = withorbs['compat'] * withorbs['stats']
	r.loc[{'stat': 'CC'}] = withorbs['CC']
	r.loc[{'stat': 'CD'}] = withorbs['CD']
	r.loc[{'stat': 'ATK_up'}] = withorbs['ATK_up']
	r.loc[{'stat': 'DEF_up'}] = withorbs['DEF_up']
	r['compat'] = withorbs['compat']
	return r.transpose('unit', 'dress', 'orb', 'stat')


def construct_model(units, dresses, orbs, orbstats, minstats, maxstats, addstats, speed_orders, s4, allow_empty=False):
	speeds = orbstats.sel(stat='AGI').values  # The speed of each slot/dress/orb combination.

	# This model is apparently best for boolean variables, as used in this problem.
	model = cp_model.CpModel()
	num_units = len(units['unit'])
	num_dresses = len(dresses['dress'])
	num_orbs = len(orbs['orb'])

	# Make a boolean for each combination of unit/dress/orb.
	u = []
	for i in range(num_units):
		d = []
		for j in range(num_dresses):
			o = []
			for k in range(num_orbs):
				o.append(model.NewBoolVar(''))
			d.append(o)
		u.append(d)

	print('\t'+_('Adding basic constraints...'))
	# For each unit, actually use the main dress.
	for i, j in np.array((units['dress'] == dresses['dress']).transpose('unit', 'dress').values.nonzero()).T:
		model.AddExactlyOne(u[i][j][k] for k in range(num_orbs))

	# 5 dresses/orbs for each unit.
	for i in range(num_units):
		if allow_empty:
			model.Add(sum(u[i][j][k] for j in range(num_dresses) for k in range(num_orbs)) <= 5)
		else:
			model.Add(sum(u[i][j][k] for j in range(num_dresses) for k in range(num_orbs)) == 5)
	# At most one unit/orb for each dress.
	for j in range(num_dresses):
		model.AddAtMostOne(u[i][j][k] for i in range(num_units) for k in range(num_orbs))
	# At most one unit/dress for each orb.
	for k in range(num_orbs):
		model.AddAtMostOne(u[i][j][k] for i in range(num_units) for j in range(num_dresses))

	print('\t'+_('Adding speed constraints...'))
	# Add speed constraints, both from order.csv and min/max_stats.csv.
	tspeeds = {units['unit'].values[i]: sum(u[i][j][k]*int(ceil(speeds[i,j,k]*SPEED_SCALE)) for j in range(num_dresses) for k in range(num_orbs)) + addstats.loc[{'stat':'AGI'}][i].item()*SPEED_SCALE for i in range(num_units)}
	apply_speed_orders(speed_orders, model, tspeeds)
	for i in units['unit'].values:
		tmin = minstats['AGI'].loc[{'unit': i}].item()
		tmax = maxstats['AGI'].loc[{'unit': i}].item()
		if tmin:
			model.Add(tspeeds[i] > tmin*SPEED_SCALE - SPEED_SCALE)
		if tmax:
			if tmin > tmax:
				raise Exception(_('Min/max stat constraints conflict.'))
			model.Add(tspeeds[i] <= tmax*SPEED_SCALE)

	print('\t'+_('Constructing stat variables...'))
	stats = {}
	for s in ['HP','ATK','DEF','FCS','RES','AGI','CC','CD','ATK_up','DEF_up']:
		statvals = orbstats.sel(stat=s).values
		stats[s] = {units['unit'].values[i]: sum(u[i][j][k]*int(ceil(statvals[i,j,k]*STAT_SCALE)) for j in range(num_dresses) for k in range(num_orbs)) + addstats.loc[{'stat':s}][i].item()*STAT_SCALE for i in range(num_units)}

	print('\t'+_('Adding min/max constraints...'))
	# Add constraints for min/max stats.
	for s in ['HP','ATK','DEF','FCS','RES','CC','CD','ATK_up','DEF_up']:
		for i in units['unit'].values:
			tmin = minstats[s].loc[{'unit': i}].item()
			tmax = maxstats[s].loc[{'unit': i}].item()
			if tmin:
				model.Add(stats[s][i] > tmin*STAT_SCALE - STAT_SCALE)
			if tmax:
				if tmin > tmax:
					raise Exception(_('Min/max stat constraints conflict.'))
				model.Add(stats[s][i] <= tmax*STAT_SCALE)

	for s in minstats.data_vars:
		if not s.startswith('bulk'):
			continue
		for i in units['unit'].values:
			stat = minstats[s].loc[{'unit': i}].item()
			if not stat:
				continue
			dfactor = parse_bulk_factor(s)
			if allow_empty:
				def0 = 400
				hp0 = 8000
				nbulk = 5
			else:
				def0 = 1000
				hp0 = 20000
				nbulk = 3
			bscale = 10
			a0 = log(hp0)
			a1 = log(stat/(1 + def0/750))
			an = np.linspace(a0, a1, nbulk+1)
			for j in range(nbulk):
				a = an[j]
				b = an[j+1]
				t = exp(a+b-log(stat))
				model.Add(stats['DEF'][i]*int(bscale*t*dfactor/750) + stats['HP'][i]*bscale >= ceil(bscale*STAT_SCALE * (exp(a) + exp(b) - t)))

	for s in maxstats.data_vars:
		if not s.startswith('bulk'):
			continue
		for i in units['unit'].values:
			stat = maxstats[s].loc[{'unit': i}].item()
			if not stat:
				continue
			dfactor = parse_bulk_factor(s)
			if allow_empty:
				def0 = 400
				hp0 = 8000
				nbulk = 5
			else:
				def0 = 1000
				hp0 = 20000
				nbulk = 3
			bscale = 100000
			a0 = log(hp0)
			a1 = log(stat/(1 + def0/750))
			bmaxvars = []
			for a in np.linspace(a0, a1, nbulk):
				t = exp(a-log(stat))
				tvar = model.NewBoolVar('')
				bmaxvars.append(tvar)
				model.Add(stats['DEF'][i]*ceil(bscale*t*dfactor/750) + stats['HP'][i]*ceil(exp(log(bscale)-a)) <= int(bscale*STAT_SCALE * (2 - t))).OnlyEnforceIf(tvar)
			model.AddBoolOr(bmaxvars)

	solver = cp_model.CpSolver()
	print('\t'+_('Model constructed.'))
	return model, solver, u, stats

# The actual optimization.

def optimize(model, solver, u, mults, orbstats, addstats, s4):
	num_units = len(mults['unit'])
	num_dresses = len(orbstats['dress'])
	num_orbs = len(orbstats['orb'])

	print('\t'+_('Constructing scores...'))
	# Maximize the sum of scores of slot/dress/orb combinations.
	scores = np.rint(xr.dot(orbstats*(1+s4), mults, dims=['stat']).values*MULTS_SCALE).astype(int)  # Unit x dress x orb x CC x CD. Bilinear interpolation is used between the extremes of CC and CD, and then one entry from each row, column, and aisle is chosen such that their sum is maximized.
	addscores = np.rint(xr.dot(addstats, mults, dims=['stat']).values*MULTS_SCALE).astype(int)
	unit_scores = [sum(scores[i,j,k] * u[i][j][k] for j in range(num_dresses) for k in range(num_orbs)) + addscores[i] for i in range(num_units)]

	print('\t'+_('Setting objective...'))
	model.Maximize(sum(unit_scores))
	print('\t'+_('Solving...'))
	status = solver.Solve(model)

	if status not in [cp_model.OPTIMAL, cp_model.FEASIBLE]:
		raise Exception(_('Solution could not be found.'))

	# List the final unit/dress/orb combinations.
	arrayresult = []
	for i in range(num_units):
		arrayresult.append([])
		for j in range(num_dresses):
			for k in range(num_orbs):
				if solver.BooleanValue(u[i][j][k]):
					arrayresult[i].append([j, k])
		arrayresult[i].extend([[-1,-1]]*(5-len(arrayresult[i])))

	# Sort the results for each unit so that the dress/orb combinations with the best stat contributions are first.
	for i in range(num_units):
		arrayresult[i] = sorted(arrayresult[i], key=lambda x: scores[i, x[0], x[1]], reverse=True)
	arrayresult = np.array(arrayresult)
	return arrayresult[:,:, 0], arrayresult[:,:, 1], [solver.Value(x)/MULTS_SCALE for x in unit_scores]

def save_csv(file, dresses, orbs, r_dresses, r_orbs):
	dresses = dresses.swap_dims({'dress': 'name'})
	# Print the results to a flat file.
	flatresult = []
	for i in range(r_orbs.shape[0]):
		for j in range(len(r_orbs[i])):
			if r_orbs[i, j] == -1:
				continue
			orb = orbs.isel(orb=r_orbs[i, j]).to_pandas()
			flatresult.append([i, dresses.isel({'name': r_dresses[i, j]})['name'].item(), orb['rarity'], orb['type'], orb['main_stat'], orb['HP'], orb['ATK'], orb['DEF'], orb['AGI'], orb['FCS'], orb['RES'], orb['HP%'], orb['ATK%'], orb['DEF%'], orb['ALL%'], orb['CC'], orb['CD'], orb['element'], orb['ATK_up'], orb['DEF_up']])
	flatresult = pd.DataFrame(flatresult, columns=['unit', 'name', 'rarity', 'type', 'main_stat', 'HP', 'ATK', 'DEF', 'AGI', 'FCS', 'RES', 'HP%', 'ATK%', 'DEF%', 'ALL%', 'CC', 'CD', 'element', 'ATK_up', 'DEF_up']).rename({'name': 'dress'})
	while True:
		try:
			flatresult.to_csv(file, index_label='slot')
		except PermissionError:
			input(_('Error: {file} in use; please close any programs using the file, then press Enter to retry.').format(file=file))
			continue
		break

def float_to_int(x):
	if isinstance(x, float):
		return int(x)
	else:
		return x

def save_yaml(path, dresses, orbs, r_dresses, r_orbs, indent=4):
	fields = ['rarity', 'type', 'main_stat', 'HP', 'ATK', 'DEF', 'AGI', 'FCS', 'RES', 'HP%', 'ATK%', 'DEF%', 'ALL%', 'CC', 'CD', 'element', 'ATK_up', 'DEF_up']
	yamlresult = {}
	for i in range(r_orbs.shape[0]):
		t_orb = orbs.isel(orb=r_orbs[i, 0]).to_pandas().to_dict()
		t_unit = {k: float_to_int(t_orb[k]) for k in fields if t_orb[k]}
		t_unit['subdresses'] = {}
		for j in range(1, r_orbs.shape[1]):
			if r_orbs[i, j] == -1:
				continue
			t_orb = orbs.isel(orb=r_orbs[i, j]).to_pandas().to_dict()
			t_unit['subdresses'][dresses['name'][r_dresses[i, j]].item()] = {k: float_to_int(t_orb[k]) for k in fields if t_orb[k]}
		yamlresult[dresses['name'][r_dresses[i, 0]].item()] = t_unit
	if isinstance(path, str):
		while True:
			try:
				with open(path, 'wb') as file:
					dump_yaml(yamlresult, file, indent=indent, sort_keys=False, encoding='utf-8', allow_unicode=True)
			except PermissionError:
				input(_('Error: {path} in use; please close any programs using the file, then press Enter to retry.').format(path=path))
				continue
			break
	else:
		dump_yaml(yamlresult, path, indent=indent, sort_keys=False, encoding='utf-8', allow_unicode=True)

def print_totals(units, stats, scores, statsfile):
	result = stats.set_index(units['name'].values)[['ATK', 'HP', 'DEF', 'AGI', 'FCS', 'RES', 'CC', 'CD', 'ATK_up', 'DEF_up']]
	result.index.names = ['dress']
	result['score'] = scores
	result['CC'] = result['CC'].map(lambda x: f'{x}%')
	result['CD'] = result['CD'].map(lambda x: f'{x}%')
	result['ATK_up'] = result['ATK_up'].map(lambda x: f'{x}%')
	result['DEF_up'] = result['DEF_up'].map(lambda x: f'{x}%')
	result.columns.name = None
	print(_('Total stats:'))
	print(tabulate(result.drop('DEF_up', axis=1), headers="keys", floatfmt='.0f'))
	while True:
		try:
			result.to_csv(statsfile, index_label='name')
		except PermissionError:
			input(_('Error: {file} in use; please close any programs using the file, then press Enter to retry.').format(file=statsfile))
			continue
		break

def save_json(path, dresses, orbs, r_dresses, r_orbs):
	result = []
	for i in range(r_orbs.shape[0]):
		result.append({'UnitNumber': i+1, 'DeckUnitSlotList': []})
		for j in range(len(r_orbs[i])):
			if r_orbs[i, j] == -1:
				continue
			orb = orbs.isel(orb=r_orbs[i, j])['UserRuneId'].item()
			dress = dresses.isel(dress=r_dresses[i, j])['UserCardId'].item()
			result[i]['DeckUnitSlotList'].append({'UnitSlotNumber': j+1, 'UserRuneId': orb, 'UserCardId': dress})

	if isinstance(path, str):
		while True:
			try:
				with open(path, 'w', encoding='utf8') as file:
					dump_json(result, file, indent='\t', ensure_ascii=False)
			except PermissionError:
				input(_('Error: {path} in use; please close any programs using the file, then press Enter to retry.').format(path=path))
				continue
			break
	else:
		dump_json(result, path, indent='\t', ensure_ascii=False)


def main(dfile, ofile, mfile=None, compatfile=None, minfile=None, maxfile=None, ordfile=None, url=None, addfile=None, yaml=None, csv=None, json=None, statsfile=None, yaml_indent=4, workers=1, filter_orbs=True, relevel=True, passes=6, solver_log=False, abslim=None, rellim=0, allow_empty=False, extras={}):
	print(__doc__)
	print(_('''Use the MGCM Dumper browser addon to get dresses.csv and orbs.csv, and save both to the same folder as this.
Then put your main units and their stat weightings into mults.csv and run this script.
Wait anywhere from 5 to 20 minutes and the results will be written to results.yaml and results.csv.
For stat constraints like setting a min speed, modify min_stats.csv and/or max_stats.csv.
For changing the constraints on the speed order of the units, modify order.csv.'''))
	print()
	print(_('Loading dresses...'))
	dresses = load_dresses(dfile)
	if relevel:
		print(_('Releveling dresses...'))
		relevel_dresses(dresses)
	compat = load_compat(compatfile, dresses)
	if url:
		print(_('Loading URL...'))
		url_data = load_url(dresses, url)
		units, mults0, linearized, s4 = url_data[0]
		speed_orders = url_data[1]
		if speed_orders is None:
			print('\t' + _('order.txt not found in Google sheet, falling back to file...'))
			speed_orders = load_speed_orders(ordfile)
	else:
		print(_('Loading mults...'))
		units, mults0, linearized, s4 = load_mults(dresses, mfile)
		print(_('Loading speed orders...'))
		speed_orders = load_speed_orders(ordfile)
	s4.loc[{'stat':'HP'}] += units['beastHP'] * 10/100
	s4.loc[{'stat':'ATK'}] += units['beastATK'] * 15/100
	s4.loc[{'stat':'DEF'}] += units['beastDEF'] * 15/100
	print(_('Loading min/max stats...'))
	minstats, maxstats = load_minmax_stats(minfile, maxfile)
	minstats = minstats.reindex({'unit':units['unit']}).fillna(0).astype(int)
	maxstats = maxstats.reindex({'unit':units['unit']}).fillna(0).astype(int)
	print(_('Loading added stats...'))
	addstats = load_add_stats(addfile, units)
	orbtypes = None
	mults = update_mults(mults0, linearized, np.maximum(minstats.to_array('stat'), addstats))
	sol_mults = [mults]
	if filter_orbs:
		orbtypes = get_filter_types(mults, minstats)
	print(_('Loading orbs...'))
	orbs = load_orbs(ofile, orbtypes)
	print(_('Applying orbs...'))
	orbstats = apply_orbs(units, dresses, orbs, compat)
	print(_('Constructing model...'))
	model, solver, modelvars, statvars = construct_model(units, dresses, orbs, orbstats, minstats, maxstats, addstats, speed_orders, s4, allow_empty)

	solver.parameters.repair_hint = True
	solver.parameters.hint_conflict_limit = 1000000000
	solver.parameters.presolve_extract_integer_enforcement = True
	solver.parameters.linearization_level = 2

	solver.parameters.num_workers = workers
	solver.parameters.log_search_progress = solver_log
	if abslim:
		solver.parameters.absolute_gap_limit = abslim * MULTS_SCALE
	solver.parameters.relative_gap_limit = rellim + 0.01*max(passes-1, 0)
	for k, v in extras.items():
		setattr(solver.parameters, k, v)

	print(_('Adding initial hints...'))
	for i, j, k in np.argwhere(orbstats['compat'].values < 0.3):
		model.AddHint(modelvars[i][j][k], False)
	print(_('Optimizing, pass 0...'))
	solutions = [optimize(model, solver, modelvars, mults, orbstats, addstats, s4)]
	time = solver.ResponseProto().deterministic_time
	r_dresses, r_orbs, scores = solutions[0]
	sol_scores = [sum(scores)]
	t_stats = pd.DataFrame({s: {i: ceil(solver.Value(t[i])*(1 + s4.loc[{'stat':s, 'unit':i}].item())/STAT_SCALE + addstats.loc[{'stat':s, 'unit':i}].item()) for i in units['unit'].values} for s, t in statvars.items()})
	t_stats['CCCD'] = t_stats['CC']*t_stats['CD']
	stats = [xr.DataArray(t_stats).rename({'dim_0': 'unit', 'dim_1': 'stat'})]
	if yaml:
		save_yaml(yaml, dresses, orbs, r_dresses, r_orbs, indent=yaml_indent)
	if csv:
		save_csv(csv, dresses, orbs, r_dresses, r_orbs)
	if json:
		save_json(json, dresses, orbs, r_dresses, r_orbs)
	final_pass = False
	for p in range(1, passes):
		model.ClearHints()
		for i in range(len(modelvars)):
			d = modelvars[i]
			for j in range(len(d)):
				o = d[j]
				for k in range(len(o)):
					model.AddHint(o[k], solver.BooleanValue(o[k]))
		print(_('Updating mults...'))
		t_mults = update_mults(mults0, linearized, stats[p-1])

		if any(all(abs(t_mults-x).values.ravel() < 0.0001) for x in sol_mults[1:]):
			final_pass = True
		solver.parameters.relative_gap_limit = rellim + 0.005*(1-final_pass)*max(passes-p-1, 0)

		sol_mults.append(t_mults)
		mults = sum(sol_mults[i+1] * sol_scores[i] for i in range(p))/sum(sol_scores)
		if final_pass:
			print(_('Optimizing, final pass...'))
		else:
			print(_('Optimizing, pass {p}...').format(p=p))
		solutions.append(optimize(model, solver, modelvars, mults, orbstats, addstats, s4))
		time += solver.ResponseProto().deterministic_time
		r_dresses, r_orbs, scores = solutions[p]
		sol_scores.append(sum(scores))
		t_stats = pd.DataFrame({s: {i: ceil(solver.Value(t[i])*(1 + s4.loc[{'stat':s, 'unit':i}].item())/STAT_SCALE + addstats.loc[{'stat':s, 'unit':i}].item()) for i in units['unit'].values} for s, t in statvars.items()})
		t_stats['CCCD'] = t_stats['CC']*t_stats['CD']
		stats.append(xr.DataArray(t_stats).rename({'dim_0': 'unit', 'dim_1': 'stat'}))
		if yaml:
			save_yaml(yaml, dresses, orbs, r_dresses, r_orbs, indent=yaml_indent)
		if csv:
			save_csv(csv, dresses, orbs, r_dresses, r_orbs)
		if json:
			save_json(json, dresses, orbs, r_dresses, r_orbs)
		if final_pass:
			break
	print()
	prestats = pd.DataFrame({s: {unit: ceil(solver.Value(t[unit])/STAT_SCALE - addstats.loc[{'stat':s, 'unit':unit}].item()) for unit in t} for s, t in statvars.items()})
	print_totals(units, prestats, scores, statsfile)
	print()
	print(_('Expected total score:') + f' {int(sum(scores))}')
	if solver_log:
		print(_('Total deterministic time:') + f' {time:.2f}')
	return time


if __name__ == "__main__":
	import traceback
	try:
		import argparse
		parser = argparse.ArgumentParser(description=_('Optimize subdresses and orbs for MGCM teams.'))

		def inFileType(path):
			try:
				return argparse.FileType('r', encoding='utf-8')(path)
			except argparse.ArgumentTypeError:
				return None

		def outFileType(path):
			try:
				return argparse.FileType('w+', encoding='utf-8')(path)
			except argparse.ArgumentTypeError:
				return None

		parser.add_argument('-d', '--outdir', type=str, dest='outdir')  # The directory to write output files to.
		parser.add_argument('-i', '--indir', type=str, dest='indir')  # The directory to look for input files.
		parser.add_argument('--dresses', type=inFileType, dest='dfile')
		parser.add_argument('--orbs', type=inFileType, dest='ofile')
		parser.add_argument('-m', '--mults', type=inFileType, dest='mfile')
		parser.add_argument('--compat', type=inFileType, dest='compatfile')
		parser.add_argument('--min', type=inFileType, dest='minfile')
		parser.add_argument('--max', type=inFileType, dest='maxfile')
		parser.add_argument('--add', type=inFileType, dest='addfile')
		parser.add_argument('--ord', type=inFileType, dest='ordfile')
		parser.add_argument('-y', '--yaml', type=outFileType, dest='yaml')
		parser.add_argument('-c', '--csv', type=outFileType, dest='csv')
		parser.add_argument('-j', '--json', type=outFileType, dest='json')
		parser.add_argument('-s', '--stats', type=outFileType, dest='statsfile')
		parser.add_argument('--yaml-indent', default=4, type=int, dest='yaml_indent')
		parser.add_argument('-w', '--workers', default=1, type=int, dest='workers')  # If you have CPU and RAM to spare, this can be increased up to the number of CPU cores on your computer.
		parser.add_argument('--filter', action='store_true', dest='filter_orbs')  # If you are a new player with a limited number of orbs, set this to False.
		parser.add_argument('--no-filter', action='store_false', dest='filter_orbs')
		parser.set_defaults(filter_orbs=True)
		parser.add_argument('--relevel', action='store_true', dest='relevel')  # Whether to simualate leveling all dresses to max level, depending on limit-break counts.
		parser.add_argument('--no-relevel', action='store_false', dest='relevel')
		parser.set_defaults(relevel=True)
		parser.add_argument('-p', '--passes', default=6, type=int, dest='passes')  # The maximum number of passes to do while optimizing for nonlinear stats.
		parser.add_argument('-l', '--lang', type=str, dest='lang')
		parser.add_argument('--log', action='store_true', dest='solver_log')  # If true, tells or-tools to print the solver log. Good for benchmarking.
		parser.add_argument('--no-log', action='store_false', dest='solver_log')
		parser.set_defaults(solver_log=False)
		parser.add_argument('-u', '--url', type=str, dest='url')
		parser.add_argument('--abslim', type=float, dest='abslim')
		parser.add_argument('--rellim', type=float, dest='rellim', default=0)
		parser.add_argument('--allow-empty', action='store_true', dest='allow_empty')
		parser.add_argument('--disallow-empty', action='store_false', dest='allow_empty')
		parser.set_defaults(allow_empty=False)
		args = vars(parser.parse_args())

		lang = args.pop('lang')
		if lang:
			translation = gettext.translation('messages', LOCALEPATH, languages=[lang])
			_ = translation.gettext

		outdir = args.pop('outdir')
		indir = args.pop('indir')
		defaults = {
			'dfile': _('dresses.csv'),
			'ofile': _('orbs.csv'),
			'mfile': _('mults.csv'),
			'compatfile': _('compat.txt'),
			'minfile': _('min_stats.csv'),
			'maxfile': _('max_stats.csv'),
			'addfile': _('add_stats.csv'),
			'ordfile': _('order.txt'),
			'yaml': _('results.yaml'),
			'csv': _('results.csv'),
			'json': _('dump.json'),
			'statsfile': _('stats.csv')
		}
		if outdir:
			outdir = outdir.rstrip('"')
			os.makedirs(outdir, exist_ok=True)
			defaults['yaml'] = os.path.join(outdir, _('results.yaml'))
			defaults['csv'] = os.path.join(outdir, _('results.csv'))
			defaults['json'] = os.path.join(outdir, _('dump.json'))
			defaults['statsfile'] = os.path.join(outdir, _('stats.csv'))
		if indir:
			indir = indir.rstrip('"')
			defaults['dfile'] = os.path.join(indir, _('dresses.csv'))
			defaults['ofile'] = os.path.join(indir, _('orbs.csv'))
			defaults['mfile'] = os.path.join(indir, _('mults.csv'))
			defaults['compatfile'] = os.path.join(indir, _('compat.txt'))
			defaults['minfile'] = os.path.join(indir, _('min_stats.csv'))
			defaults['maxfile'] = os.path.join(indir, _('max_stats.csv'))
			defaults['addfile'] = os.path.join(indir, _('add_stats.csv'))
			defaults['ordfile'] = os.path.join(indir, _('order.txt'))

		for k,v in defaults.items():
			if (k not in args) or (not args[k]):
				args[k] = defaults[k]

		time = main(**args)
		print()
		input(_('Optimization done, press Enter to exit.'))
	except:
		print(traceback.format_exc())
		input(_('Error encountered, press Enter to exit.'))
