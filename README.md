# MGCM Optimizer

Python script to choose optimal subdresses and orbs for MGCM teams.

## Getting started

### Setup

1. Download `optimize.exe`, `db.csv`, `order.csv`, `min_stats.csv`, `max_stats.csv`, and `mults.csv`, all into a new folder for the Optimzer.
2. Use the MGCM Dumper browser addon to download `dresses.csv` and `orbs.csv`, and save both to the same folder as the other files.

## Usage

Actually running the program is simple: put all the files in the same folder, double-click `optimize.exe`, and wait for it to finish.
The results will be saved to `results.yaml` and `results.csv`; both are plain text files listing each subdress, and the stats for the orb to attach to it.

A file named `stats.csv` will also be written, giving the total stats of each optimized unit, including omnis bonuses.
The stat values in it can be easily copy-pasted into the Statistics page of the Mults Calculator sheet by opening it with LibreOffice Calc or Microsoft Excel, and using `Ctrl-C`, `Ctrl-Shift-V` to paste it without formatting.

The harder part is filling in the rest of the files, as described below.

### mults.csv

If you are using a relatively standard comp such as Daru-Ifrit, another player is likely to already have a `mults.csv` ready for you to use.

Otherwise, it can be calculated using the [Mults calculator spreadsheet](https://docs.google.com/spreadsheets/d/1PkdDIzgls7xvjctRSMuGRfVERzLTkhH0e1qrisFOFaE/edit?usp=sharing) and following the instructions on the first page.
Essentially, it adds up all the skill mods for the given turn order, taking into account buffs and debuffs.

The other use case is when you want to maximize a certain stat at all costs (except for the constraints set in the other files).
For example, to see how fast a dress can possibly get for sabbath, remove all the other rows, set the AGI field to 1, and the rest to 0.

Beware, if trying to give a dress a good balance of stats, that should be done in conjunction with the `min_stats.csv` file; `mults.csv` by itself will gladly just maximize one or the other.

The `CC-*`/`CD-*`/`CCCD-*` describe the impact of the CC/CD stats on the *other* stats; while the mults for the primary stats can be estimated with mock runs if needed, these columns are required for the optimizer to properly consider crit orbs.

Finally, the optimizer also supports the columns `bulk`, `bulk-dd`, and `bulk-du`: these correspond to effective HP, meaning the combination of HP and DEF of the dresses. `bulk-dd` is with DEF-down applied, `bulk-du` is with DEF-up applied.

### order.csv

This file is what forces a certain turn order on the dresses.
Each row should look like `a>b` or `a,b`; both mean that the unit in slot `a` is to be faster than the unit in slot `b`.

For almost all teams, `order.csv` should just be these 3 lines, assuming the speed order is from left to right:
```
0>1
1>2
2>3
```

This is the most important file other than `mults.csv`, and maximizing the damage while restricted by this turn order is the real advantage of the optimizer: it regularly gives results with each dress less than 2 SPD faster than the next.

### add_stats.csv

This file is where flat stats boosts like from Omnis enhancement or FCS/RES/AGI S4s should go.
This is most-important when you want to enforce a speed order when only some dresses are boosted by an S4, but also has a small impact with crit stats in optimization, and improves the damage estimation accuracy.

### min_stats.csv

This file is where minimum stat requirements are set.
This is most-useful for sabbath, where a minimum speed is needed to link with a gauger, and where many dresses want to maximize ATK while maintaining a certain amount of FCS.
It is also useful for later rounds of SF, to make sure that you're faster than the boss, or to make the buffer substantially faster than the other dresses.

### max_stats.csv

The least-important file, for the vast majority of teams every entry should be zero.

The first use case is endless or SF, when you want a dress to be slower than the boss:
In endless, having Hanabi slower can mean a higher damage cap; in SF, it can mean higher debuff uptime for things like R1 Ao's silence.

The other niche use is with sacrificial dresses like Brunhild Lilly, where you want to put a cap on the HP and/or DEF to make sure it dies easily.

### db.csv

This file contains the lvl1 and lvl80 stats for (almost) every dress, and shouldn't be modified unless it's out-of-date.

When present, this allows the optimizer to interpolate between dresses' lvl1 and lvl80 stats to consider them at any level. Required for the `RELEVEL_DRESSES` option to work.

## Other options

### Command-line flags

These can be used by running the optimizer from the command-line; the easiest way to do so on Windows is to hold Shift and right-click on the folder, click "Open PowerShell window here, then type in, for example, `.\optimize.exe -m team1.csv -y team1_results.yaml`.

Most command-line flags for for using a non-default filename for input/output files:
* `-d`, `--dresses`: `dresses.csv`
* `-o`, `--orbs`: `orbs.csv`
* `-m`, `--mults`: `mults.csv`
* `--db`: `db.csv`
* `--min`: `min_stats.csv`
* `--max`: `max_stats.csv`
* `--add`: `add_stats.csv`
* `--ord`: `order.csv`
* `-y`, `--yaml`: `results.yaml`, yaml-formatted results.
* `-c`, `--csv`: `results.csv`, csv-formatted results.
* `-s`, `--stats`: `stats.csv`, the total stats of the optimized team, csv-formatted.

There are also some runtime constants that can be configured:
* `--filter`, `--no-filter`: (Default: `True`) When enabled, only looks at the orbs of types that have nonzero entries in mults or min_stats.csv.
Because of the speed order constraints however, it always considers speed orbs too. This should be disabled if you are a new player and don't have many orbs.
* `--relevel`, `--no-relevel`: (Default: `True`) When enabled, considers all dresses as if they are fully-leveled for their current limi-break count. This should be disabled if you want to use a dress like, say, Brunhild Lilly that you want to keep at low level.
* `-w`, `--workers`: (Default: `2`) Essentially, how many threads it should use. You should leave it below the number of cores your computer has, or your computer will become extremely slow and unusable.
This can also be lowered if you need the optimizer to use less RAM, or increased if you have computing power to spare.
* `-p`, `--passes`: (Default: `6`) For nonlinear mults like for the crit stats or for bulk, this is the maximum number of iterations to spend optimizing. If you are doing, say, a speed optimization, you might as well just set this to `1` to save time.
* `--yaml-indent`: (Default: `4`) The number of spaces to use for indents in `results.yaml`.


### Additional script constants

These require installing the python dependencies yourself (using `pip install --upgrade --user numpy pandas xarray ortools pyyaml` for example), and running `optimize.py` instead of `optimize.exe`.
* `MULTS_SCALE`: (Default: `100`) This determines how precisely you can specify the values in `mults.csv`; 1000 = three decimal places.
* `SPEED_SCALE`, `STAT_SCALE`: (Default: `20`)These are because of the 20%/25%/30% contribution from each subdress; a x20 multiplier means that there is no incorrect rounding.
`SPEED_SCALE` is used for calculating the speed order / min-speed constraints, `STAT_SCALE` is for the other constraints, and for the actual optimization.


## Copyright and license

MGCM Optimizer
Copyright (C) 2021  Aeodyn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
