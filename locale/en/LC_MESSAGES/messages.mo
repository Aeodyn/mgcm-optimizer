��    )      d  ;   �      �     �     �     �     �                -  Z   L     �     �     �     �     �               /  "   G     j  '   }  ,   �     �     �                +  
   H     S     `  �  r     D     R     Y     e     s  	   �     �  	   �     �     �  	   �  3  �     �	     
     /
     K
     \
     r
     �
  Z   �
               0     >     Q     j     {     �  "   �     �  '   �  ,        .     D     \     r     �  
   �     �     �  �  �     �     �     �     �     �  	   �     �  	   �     �       	                     	                                
                                       (       !   '          "       %                       &      #              $          )                              Adding basic constraints... Adding min/max constraints... Adding speed constraints... Applying orbs... Constructing model... Constructing scores... Constructing stat variables... Error: {file} in use; please close any programs using the file, then press Enter to retry. Expected total score: Loading added stats... Loading db... Loading dresses... Loading min/max stats... Loading mults... Loading orbs... Loading speed orders... Min/max stat constraints conflict. Model constructed. Optimization done, press Enter to exit. Optimize subdresses and orbs for MGCM teams. Optimizing, pass 0... Optimizing, pass {p}... Releveling dresses... Setting objective... Solution could not be found. Solving... Total stats: Updating mults... Use the MGCM Dumper browser addon to get dresses.csv and orbs.csv, and save both to the same folder as this.
Then put your main units and their stat weightings into mults.csv and run this script.
Wait anywhere from 5 to 20 minutes and the results will be written to results.yaml and results.csv.
For stat constraints like setting a min speed, modify min_stats.csv and/or max_stats.csv.
For changing the constraints on the speed order of the units, modify order.csv. add_stats.csv db.csv dresses.csv max_stats.csv min_stats.csv mults.csv orbs.csv order.txt results.csv results.yaml stats.csv Project-Id-Version: MGCM Optimizer
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-05 01:03-0500
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Adding basic constraints... Adding min/max constraints... Adding speed constraints... Applying orbs... Constructing model... Constructing scores... Constructing stat variables... Error: {file} in use; please close any programs using the file, then press Enter to retry. Expected total score: Loading added stats... Loading db... Loading dresses... Loading min/max stats... Loading mults... Loading orbs... Loading speed orders... Min/max stat constraints conflict. Model constructed. Optimization done, press Enter to exit. Optimize subdresses and orbs for MGCM teams. Optimizing, pass 0... Optimizing, pass {p}... Releveling dresses... Setting objective... Solution could not be found. Solving... Total stats: Updating mults... Use the MGCM Dumper browser addon to get dresses.csv and orbs.csv, and save both to the same folder as this.
Then put your main units and their stat weightings into mults.csv and run this script.
Wait anywhere from 5 to 20 minutes and the results will be written to results.yaml and results.csv.
For stat constraints like setting a min speed, modify min_stats.csv and/or max_stats.csv.
For changing the constraints on the speed order of the units, modify order.csv. add_stats.csv db.csv dresses.csv max_stats.csv min_stats.csv mults.csv orbs.csv order.txt results.csv results.yaml stats.csv 